package com.udemy.SpringBoot;

import com.udemy.SpringBoot.domain.Category;
import com.udemy.SpringBoot.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class Application implements CommandLineRunner {

	@Autowired
	private CategoryRepository categoryRepository;

	public static void main(String[] args) {
		SpringApplication.run(Application.class, args);
	}

	@Override
	public void run(String... args) throws Exception {

		categoryRepository.saveAll(Arrays.asList(
				new Category(null, "IT"),
				new Category(null, "Vehicle"),
				new Category(null, "Office")
		));
	}
}
