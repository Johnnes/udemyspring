package com.udemy.SpringBoot.services;

import com.udemy.SpringBoot.domain.Category;
import com.udemy.SpringBoot.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;

    public Category find(Integer id){
        Optional<Category> category = categoryRepository.findById(id);
        return category.orElse(null);
    }

}
